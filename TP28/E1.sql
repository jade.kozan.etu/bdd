DROP TABLE import;

CREATE TABLE import (
n0 text, n1 text, n2 text, n3 text, n4 text, n5 text, n6 text, n7 int, n8 int, n9 text, n10 int,
n11 text, n12 int, n13 text, n14 text, n15 int, n16 text, n17 text, n18 int, n19 text, n20 text,
n21 int, n22 text, n23 text, n24 text, n25 int, n26 text, n27 text,
n28 int, n29 text, n30 text, n31 text, n32 int, n33 text, n34 text,
n35 int, n36 text, n37 text, n38 text, n39 int, n40 text, n41 text,
n42 int, n43 text, n44 text, n45 text, n46 int, n47 text, n48 text,
n49 int, n50 text, n51 text, n52 text, n53 int, n54 text, n55 text,
n56 int, n57 text, n58 text, n59 text, n60 int, n61 text, n62 text,
n63 int, n64 text, n65 text, n66 text, n67 int, n68 text, n69 text,
n70 int, n71 text, n72 text, n73 text, n74 int, n75 text, n76 text,
n77 int, n78 text, n79 text, n80 text, n81 int, n82 text, n83 text,
n84 int, n85 text, n86 text, n87 text, n88 int, n89 text, n90 text,
n91 int, n92 text, n93 text, n94 text, n95 int, n96 text, n97 text);

SELECT reltuples FROM pg_class
WHERE relname = 'import'
AND relowner=(SELECT usesysid FROM pg_user WHERE usename=USER);