ALTER TABLE departements ADD PRIMARY KEY(dno);
ALTER TABLE communes ADD PRIMARY KEY(dno,cno);
ALTER TABLE bureaux ADD PRIMARY KEY(dno,cno,bno);
ALTER TABLE candidats ADD PRIMARY KEY(cand);
ALTER TABLE votes ADD PRIMARY KEY(dno,cno,bno,cand);
ALTER TABLE communes ADD FOREIGN KEY (dno) REFERENCES departements(dno);
ALTER TABLE bureaux ADD FOREIGN KEY (dno,cno) REFERENCES communes(dno,cno);
ALTER TABLE votes ADD FOREIGN KEY (dno,cno,bno) REFERENCES bureaux(dno,cno,bno);
ALTER TABLE votes ADD FOREIGN KEY (cand) REFERENCES candidats(cand);