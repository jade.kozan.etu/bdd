DROP TABLE scores;

CREATE TABLE scores(logins text, id int
constraint fk_id references jeu(id),score int);

INSERT INTO scores
VALUES
('jadekozanetu',1,20),
('jadekozanetu',2,8),
('jadekozanetu',3,26),
('cedriclarsonnieretu',1,17),
('cedriclarsonnieretu',2,13),
('cedriclarsonnieretu',3,9);