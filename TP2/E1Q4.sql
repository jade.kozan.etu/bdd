DROP VIEW messcores;

CREATE VIEW messcores 
AS SELECT * FROM scores
WHERE logins = USER;

SELECT * FROM messcores;