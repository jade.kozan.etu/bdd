SELECT ville FROM personne
GROUP BY ville
HAVING COUNT(*) <= ALL (SELECT COUNT(*) FROM personne GROUP BY ville);