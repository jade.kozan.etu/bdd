DROP INDEX IF EXISTS i_pers;

CREATE INDEX i_pers
ON personne (nom);

SELECT pg_size_pretty(pg_indexes_size('personne'));