DROP TABLE compagnies;

CREATE TABLE compagnies (compagnie text);

INSERT INTO compagnies (compagnie)
SELECT DISTINCT compagnie FROM avion
WHERE compagnie IS NOT NULL;