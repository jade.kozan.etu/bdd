    DROP SEQUENCE lig;
    CREATE SEQUENCE lig START WITH 1000;

INSERT INTO ligne (lno, depart,arrivee)
SELECT nextval('lig'), a1.rno, a2.rno
FROM aeroport AS a1, aeroport AS a2
WHERE a1.rno <> a2.rno;