INSERT INTO aeroport(rno, libelle, ville)
VALUES ('JFK', 'John Fitzgerald Kennedy', 'New-York') ;
INSERT INTO aeroport(rno, libelle,ville)
VALUES ('CDG', 'Roissy Charles de Gaulles','Paris') ;
INSERT INTO aeroport(rno, libelle,ville)
VALUES ('MAD', 'Madrid Barajas','Madrid') ;
INSERT INTO aeroport(rno, libelle) VALUES ('BRU', 'Bruxelles') ;
INSERT INTO aeroport(rno, libelle) VALUES ('GVA', 'Genève') ;
INSERT INTO aeroport(rno, libelle, ville)
VALUES ('ORY', 'Orly', 'Paris') ;
INSERT INTO aeroport(rno, libelle) VALUES ('LAI', 'Lannion') ;
INSERT INTO aeroport(rno, libelle, ville)
VALUES ('LIL', 'Lille-Lesquin', 'Lille') ;

UPDATE aeroport
SET ville = libelle
WHERE ville IS NULL;