DROP TABLE modeles;

CREATE TABLE modeles (types text, places integer);

INSERT INTO modeles (types,places)
SELECT DISTINCT types,MAX(places) FROM avion
GROUP BY types;