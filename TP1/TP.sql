drop table if exists aeroport cascade;
drop table if exists ligne cascade;
drop table if exists vol;
drop table if exists avion;
drop table if exists pilote;



create table avion(
    ano int, types text, places int constraint c1 check (places between 100 and 500),compagnie text,
    constraint pk_avion primary key(ano)
);

create table pilote(
    pno serial, 
    nom text not null,
    prenom text not null,
    adresseP text not null,
    constraint pk_pilote primary key(pno)
);

create table aeroport(
    rno char(3), 
    libelle text,
    ville text,
    adresseA text,
    constraint pk_aeroport primary key (rno)
);

create table ligne(
    lno int,
    depart char(3), 
    arrivee char(3),
    constraint pk_ligne primary key(lno),
    constraint fk_aeroport1 foreign key (depart) references aeroport(rno) on update cascade on delete set null,
    constraint fk_aeroport2 foreign key (arrivee) references aeroport(rno) on update cascade on delete set null
);

create table vol(
    ano int,
    pno int,
    lno int,
    hdepart time,
    harrivee time,
    constraint pk_vol primary key(ano,lno,pno),
    constraint fk_avion foreign key(ano) references avion(ano) on update cascade on delete cascade,
    constraint fk_ligne foreign key(lno) references ligne(lno) on update cascade on delete cascade,
    constraint fk_pilote foreign key(pno) references pilote(pno) on update cascade on delete cascade
);
