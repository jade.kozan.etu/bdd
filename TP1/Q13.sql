DELETE FROM vol;

INSERT INTO vol (ano, pno,lno)
SELECT a.ano, p.pno, l.lno FROM avion AS a, pilote AS p, ligne AS l
WHERE a.compagnie = 'AIR FRANCE'
AND a.types LIKE 'A%'
AND p.pno %7 = 0
AND (l.depart IN('CDG','JFK','MAD')
OR l.arrivee IN('CDG','JFK','MAD'));