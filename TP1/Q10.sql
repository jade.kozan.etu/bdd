CREATE SEQUENCE numeros START 130;

INSERT INTO avion(ano,types,places,compagnie)
SELECT nextval('numeros'),m.types,m.places,c.compagnie FROM modeles AS m, compagnies AS c;