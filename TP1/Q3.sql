drop table noms;
drop table prenoms;
drop table adresses;

CREATE TEMPORARY TABLE noms(nom text);
CREATE TEMPORARY TABLE prenoms(prenom text);
CREATE TEMPORARY TABLE adresses(adresse text);

INSERT INTO noms
VALUES ('Haddock'),('Tournesol'),('Bergamotte'),('Lampion');

INSERT INTO prenoms
VALUES ('Tryphon'),('Archibald'),('Hippolyte'),('Seraphin');

INSERT INTO adresses
VALUES ('Moulinsart'),('Sbrodj'),('Bruxelles'),('Klow');

