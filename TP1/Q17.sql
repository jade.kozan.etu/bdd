DROP TABLE IF EXISTS correspondances

CREATE TABLE correspondances (depart, arrivee, escale, lno1,lno2)
AS SELECT l1.depart, l2.depart,l1.arrivee,l1.lno,l2.lno
FROM ligne AS l1, ligne AS l2
WHERE l1.arrivee <> l2.depart
AND NOT EXISTS
(SELECT  * FROM ligne AS l3 WHERE l3.depart = l1.depart AND l3.arrivee = l2.arrivee);